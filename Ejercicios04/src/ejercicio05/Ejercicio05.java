package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		/*
		 * 5) Programa que lea un mes en tipo String y compruebe si el valor corresponde
		 * a un mes de 30, 31 o 28 d�as. Se mostrar� adem�s el nombre del mes. Comprobar
		 * si la opci�n introducida no es v�lida. (No es necesario poner break; en todos
		 * los casos.)
		 */

		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce un mes en texto");
		String mes = sc.nextLine();

		String mesminuscula=mes.toLowerCase();
		switch (mesminuscula) {
		case "enero":
		case "marzo":
		case "mayo":
		case "julio":
		case "agosto":
		case "octubre":
		case "diciembre":
			System.out.println("Tu mes " + mes + " tiene 31 d�as");
			break;
		case "abril":
		case "junio":
		case "septiembre":
		case "noviembre":
			System.out.println("Tu mes " + mes + " tiene 30 d�as");
			break;
		case "febrero":
			System.out.println("Tu mes " + mes + "puede tener 29 o 28 d�as");
			break;
		default: System.out.println("no has introducido un mes");
		}

		sc.close();

	}

}
