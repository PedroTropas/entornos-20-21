package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*
		 * 4) Pedir una nota num�rica entera entre 0 y 10 y mostrar el valor de la nota:
		 * Insuficiente, Suficiente, Bien, Notable o Sobresaliente. (No es necesario
		 * poner break; en todos los casos.)
		 */

		Scanner sc = new Scanner(System.in);
		System.out.println("introduce el numero de nota entre 0 y 10");
		int nota = sc.nextInt();

		switch (nota) {
		case 0:

		case 1:

		case 2:

		case 3:

		case 4:
			System.out.println("Tu nota es insuficiente");
			break;
		case 5:
			System.out.println("Tu nota es la suficiente");
			break;
		case 6:

		case 7:
			System.out.println("Tu nota es un bien");
			break;
		case 8:

		case 9:
			System.out.println("Tu nota es un notable");
			break;
		case 10:
			System.out.println("tu nota es sobresaliente");
			break;
		default:
			System.out.println("El numero introducido se sale de los valores");
		}
		sc.close();

	}

}
