package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {

		/*
		 * 1) 1a. Programa Java que lea un car�cter en min�scula (no hace falta
		 * comprobarlo) y, utilizando una sentencia switch, diga qu� vocal es, o en caso
		 * contrario, que indique que no es vocal, y muestre el car�cter en ambos casos.
		 * Probar el ejercicio usando la sentencia break en los cases. 1b Mismo
		 * ejercicio, pero sin usar sentencia break en los �cases�. Mostrar un mensaje
		 * al final del programa explicando qu� ocurre.
		 */

		Scanner sc = new Scanner(System.in);
		System.out.println("introduce una letra minuscula");
		char caracter = sc.nextLine().charAt(0);

		int numero = (int) caracter;

		switch (numero) {
		case 97:
			System.out.println("Tu letra es la a");
			break;
		case 101:
			System.out.println("Tu letra es la e");
			break;
		case 105:
			System.out.println("Tu letra es la i");
			break;
		case 111:
			System.out.println("Tu letra es la o");
			break;
		case 117:
			System.out.println("Tu letra es la a");
			break;
		default:
			System.out.println("Tu letra no es una vocal minuscula has introducido: " + caracter);
		}

		switch (caracter) {
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			System.out.println("tu letra minuscula es: " + caracter);
			break;
		default:
			System.out.println("Tu letra no es una vocal minuscula has introducido: " + caracter);
		}

		sc.close();
	}

}
