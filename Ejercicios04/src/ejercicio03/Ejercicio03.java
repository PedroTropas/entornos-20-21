package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		/*
		 * 3) Pedir una nota num�rica entera entre 0 y 10, y mostrar dicha nota de la
		 * forma: cero, uno, dos, tres... Comprobar si la opci�n introducida no es
		 * v�lida.
		 */

		Scanner sc = new Scanner(System.in);
		System.out.println("introduce el numero de nota entre 0 y 10");
		int nota = sc.nextInt();

		switch (nota) {
		case 0:
			System.out.println("La nota elegida es la 0");
			break;
		case 1:
			System.out.println("La nota elegida es la 1");
			break;
		case 2:
			System.out.println("La nota elegida es la 2");
			break;
		case 3:
			System.out.println("La nota elegida es la 3");
			break;
		case 4:
			System.out.println("La nota elegida es la 4");
			break;
		case 5:
			System.out.println("La nota elegida es la 5");
			break;
		case 6:
			System.out.println("La nota elegida es la 6");
			break;
		case 7:
			System.out.println("La nota elegida es la 7");
			break;
		case 8:
			System.out.println("La nota elegida es la 8");
			break;
		case 9:
			System.out.println("La nota elegida es la 9");
			break;
		case 10:
			System.out.println("La nota elegida es la 10");
			break;
		default:
			System.out.println("El numero introducido se sale de los valores");
		}
		sc.close();
	}

}
