package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		/*
		 * 6) Programa que pida 2 n�meros float y a continuaci�n un car�cter de
		 * operaci�n aritm�tica (+, -, *, /, %). Mediante una sentencia switch escoger
		 * el caso referente al car�cter de operaci�n introducido. El programa mostrar�
		 * un mensaje con el resultado de la operaci�n escogida, entre los 2 n�meros.
		 * Indicar si el car�cter introducido no es v�lido.
		 */

		Scanner sc = new Scanner(System.in);
		System.out.println("introduce el primer numero");
		float numero1 = sc.nextFloat();
		System.out.println("introduce el segundo numero");
		float numero2 = sc.nextFloat();
		System.out.println("Introduce un caracter para hacer la cuenta(+,-,*,/,%)");
		sc.nextLine();
		char caracter = sc.nextLine().charAt(0);
		switch (caracter) {
		case '+':
			System.out.println("La suma es :" + (numero1 + numero2));
			break;
		case '-':
			System.out.println("La resta es :" + (numero1 - numero2));
			break;
		case '*':
			System.out.println("La multiplicacion es :" + (numero1 * numero2));
			break;
		case '/':
			System.out.println("La division es :" + (numero1 / numero2));
			break;
		case '%':
			System.out.println("El resto es :" + (numero1 % numero2));
			break;
		default:
			System.out.println("El caracter que has introducido no es valido");
		}
		sc.close();
	}

}
