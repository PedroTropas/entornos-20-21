package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		/*
		 * 2) Programa que pida al usuario una cantidad de gramos y posteriormente pida
		 * seleccionar una opci�n de un men� con opciones num�ricas (opciones del men�:
		 * 1-Kilogramo, 2- Hectogramos, 3-Decagramos, 4-Decigramos, 5-Centigramos,
		 * 6-Miligramos). Al seleccionar una opci�n mostrar la conversi�n de gramos a
		 * dicha unidad. Comprobar si la opci�n del men� introducida no es v�lida. Si la
		 * cantidad de gramos es un valor negativo, no hacer nada.
		 */

		Scanner sc = new Scanner(System.in);
		System.out.println("introduce tu cantidad de gramos");

		double gramos = sc.nextDouble();
		if (gramos < 0) {
			System.out.println("El numero de gramos es negativo");
		} else {

			System.out.println("introduce (opciones del men�:");
			System.out.println("1-Kilogramo");
			System.out.println("2-Hectogramos");
			System.out.println("3-Decagramos,");
			System.out.println("4-Decigramos");
			System.out.println("5-Centigramos");
			System.out.println("6-Miligramos");

			/*
			 * System.out.println(
			 * "introduce (opciones del men�:1-Kilogramo, 2- Hectogramos, 3-Decagramos, 4-Decigramos, 5-Centigramos,6-Miligramos)"
			 * );
			 */int opcion = sc.nextInt();

			switch (opcion) {
			case 1:
				System.out.println("total: " + (gramos / 1000) + " Kilogramos");
				break;
			case 2:
				System.out.println("total: " + (gramos / 100) + " Hectogramos");
				break;
			case 3:
				System.out.println("total: " + (gramos / 10) + " Decagramos");
				break;
			case 4:
				System.out.println("total: " + (gramos * 10) + " Decigramos");
				break;
			case 5:
				System.out.println("total: " + (gramos * 100) + " Centigramos");
				break;
			case 6:
				System.out.println("total: " + (gramos * 100) + " Miligramos");
				break;
			default:
				System.out.println("El numero que has introducido en el menu es un numero que no tiene una opcion");
			}
		}
		sc.close();
	}

}
